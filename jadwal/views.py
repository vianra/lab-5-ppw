from django.shortcuts import render, redirect

from .models import Jadwal
from .forms import JadwalForm


def create(request):
        jadwal_form = JadwalForm(request.POST or None)
        if request.method == 'POST':
                if jadwal_form.is_valid():
                        jadwal_form.save()
                return redirect('jadwal:list')

        context = {
                "page_title": "Tambah jadwal",
                "jadwal_form": jadwal_form,
        }

        return render(request, 'create.html', context)

def delete(request, delete_id):
        Jadwal.objects.filter(id=delete_id).delete()
        return redirect('jadwal:list')

def change(request, change_id):
        jadwal_change = Jadwal.objects.get(id=change_id)
        ubah = {
                'hari' : jadwal_change.hari,
                'tanggal' : jadwal_change.tanggal,
                'jam' : jadwal_change.jam,
                'nama_kegiatan' : jadwal_change.nama_kegiatan,
                'tempat' : jadwal_change.tempat,
                'kategori' : jadwal_change.kategori,
        }
                
        jadwal_form = JadwalForm(request.POST or None, initial=ubah, instance=jadwal_change)
        if request.method == 'POST':
                if jadwal_form.is_valid():
                        jadwal_form.save()
                return redirect('jadwal:list')

        context = {
                "page_title": "Tambah jadwal",
                "jadwal_form": jadwal_form,
        }

        return render(request, 'create.html', context)

def list(request):
	semua_jadwal = Jadwal.objects.all()

	context = {
		'page_title' : 'Jadwal Kegiatan Saya',
		'semua_jadwal' : semua_jadwal,
	}

	return render(request, 'list.html', context)
