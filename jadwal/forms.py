from django import forms

from .models import Jadwal

class JadwalForm(forms.ModelForm):
	class Meta:
		model = Jadwal
		fields = [
			'hari',
			'tanggal',
			'jam',
			'nama_kegiatan',
			'tempat',
			'kategori',
		]
