from django.db import models

# Create your models here.
class Jadwal(models.Model):
	hari = models.CharField(max_length=100)
	tanggal = models.DateField()
	jam = models.TimeField()
	nama_kegiatan = models.CharField(max_length=100)
	tempat = models.CharField(max_length=100)
	kategori = models.CharField(max_length=100)

	def __str__(self):
		return "{}.{}".format(self.id, self.nama_kegiatan)