from django.shortcuts import render

# Create your views here.
def home(request):
	context = {
	"page_title": "Homepage",
	}
	return render(request, 'homepage.html', context)

def about(request):
	context = {
	"page_title": "About",
	}
	return render(request, 'aboutpage.html', context)

def skills(request):
	context = {
	"page_title": "Skills",
	}
	return render(request, 'skillspage.html', context)

def interests(request):
	context = {
	"page_title": "Interests",
	}
	return render(request, 'interestspage.html', context)

def contact(request):
	context = {
	"page_title": "Contact",
	}
	return render(request, 'contactpage.html', context)
