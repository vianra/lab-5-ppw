from django.conf.urls import url

from . import views

urlpatterns = [
        url(r'^$', views.home, name='home'),
        url(r'^about/$', views.about, name='about'),
        url(r'^skills/$', views.skills, name='skills'),
        url(r'^interests/$', views.interests, name='interests'),
        url(r'^contact/$', views.contact, name='contact'),
]
